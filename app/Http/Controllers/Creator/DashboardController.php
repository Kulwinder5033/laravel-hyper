<?php

namespace App\Http\Controllers\Creator;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:creator');
    }

    public function index()
    {
        return view('creator.dashboard.dashboard');
    }
}
